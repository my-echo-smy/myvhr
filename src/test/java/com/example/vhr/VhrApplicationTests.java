package com.example.vhr;

import com.example.vhr.mapper.HrMapper;
import com.example.vhr.model.Hr;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class VhrApplicationTests {

	@Autowired
	HrMapper hrMapper;
	@Test
	void contextLoads() {
		Hr hr = hrMapper.selectByPrimaryKey(3);
		System.out.println(hr.getAddress());
	}


}
