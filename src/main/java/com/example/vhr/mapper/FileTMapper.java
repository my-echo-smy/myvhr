package com.example.vhr.mapper;

import com.example.vhr.model.Employee;
import com.example.vhr.model.FileT;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface FileTMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(FileT record);

    FileT selectByPrimaryKey(String id);

    List<FileT> getEmployeeByPageWithSalary(@Param("page") Integer page, @Param("size") Integer size);

    Long getTotal(@Param("emp") Employee employee, @Param("beginDateScope") Date[] beginDateScope);
}
