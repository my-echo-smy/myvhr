package com.example.vhr.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SysConfigFile
{
  public static Logger logger = LoggerFactory.getLogger(SysConfigFile.class);

  private static SysConfigFile instance = null;
  private static final String FilePath = "/system.properties";
  private static Properties prop;

  private SysConfigFile()
  {
    InputStream in = null;
    try {
      in = SysConfigFile.class.getResourceAsStream(FilePath);
      prop = new Properties();
      prop.load(in);
    }
    catch (Exception e) {
      logger.error("Read "+FilePath+" IOException:");
      try
      {
        if (in != null)
          in.close();
      }
      catch (IOException e2) {
        logger.error("Read "+FilePath+" IOException:");
      }
    }
    finally
    {
      try
      {
        if (in != null)
          in.close();
      }
      catch (IOException e) {
        logger.warn("Read "+FilePath+" IOException:");
      }
    }
  }

  public static SysConfigFile getInstance() {
    if (instance == null) {
      instance = new SysConfigFile();
    }
    return instance;
  }

  public static String getValue(String key) {

	     getInstance();
	     if(prop.get(key)!=null){
	    	 
	    	 return (String)prop.get(key);
	     }else{
	    	 
	    	 return null;
	     }
	    
   
  }

  public Properties getProperties() {
    return prop;
  }

  public static void main(String[] args) {
    SysConfigFile c1 = getInstance();
    System.out.println(c1.getProperties());
    System.out.println(c1.getValue("你"));
  }
}