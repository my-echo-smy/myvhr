
package com.example.vhr;

import freemarker.template.TemplateException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactory;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;
import java.util.Properties;

/**
 * 模板处理
 * <p>
 * cn.gwssi.manager.service.impl
 * </p>
 * <p>
 * File: FreeMakerConfigure.java 创建时间: 2020-01-15 10:15:06
 * </p>
 * <p>
 * Title: []_[]
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * 模块: -
 * </p>
 *
 * @author simengyuan
 * @history 修订历史（历次修订内容、修订人、修订时间等）
 **/
@Configuration
public class FreeMakerConfigure {
    @Bean
    public FreeMarkerConfigurer freemarkerConfig() throws IOException, TemplateException {
        FreeMarkerConfigurationFactory factory = new FreeMarkerConfigurationFactory();
        factory.setTemplateLoaderPath("classpath:/templates/");
        factory.setDefaultEncoding("UTF-8");
        factory.setPreferFileSystemAccess(true);
        FreeMarkerConfigurer result = new FreeMarkerConfigurer();
        freemarker.template.Configuration configuration = factory.createConfiguration();
        configuration.setClassicCompatible(true);
        result.setConfiguration(configuration);
        Properties settings = new Properties();
        settings.put("template_update_delay", "0");
        settings.put("default_encoding", "UTF-8");
        settings.put("number_format", "0.######");
        settings.put("classic_compatible", true);
        settings.put("template_exception_handler", "ignore");
        result.setFreemarkerSettings(settings);
        return result;
    }

}
